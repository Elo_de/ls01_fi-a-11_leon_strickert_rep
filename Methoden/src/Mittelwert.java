import java.util.Scanner;
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ==========================
      
	   double x = eingabex();
	   double y = eingabey();
	   double m = rechnung();
	   
	   
	   
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      
      
   }
   public static double eingabex() {
	   
	   Scanner tastatur= new Scanner(System.in);
	   System.out.println("Geben Sie mir einen x-Wert");
	      double x = tastatur.nextDouble();
	      return x;
	     
   }
   public static double eingabey() {
	  
	   Scanner tastatur= new Scanner(System.in);
	   System.out.println("Geben Sie mir einen y-Wert");
	      double y = tastatur.nextDouble();
	   return y;
   }
   public static double rechnung(double x,double y) {
	 
	  double m = (x + y) / 2.0;
	  return m;
   }
   
}

